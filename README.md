BRUTE FORCE IN NODE JS
This script can be used in load testing of a server
and to retrive the password if someone has forgotten.

This script calculate the all the combination of given word of array and start hitting the server
one the password get matched it get stop.

There are few changes in script user has to do:
1.  var config 	=	{	
						target_url 		:  	"http://example.com/user/login.php", // target url where data need to be posted
						max_length 		: 	5, // need to set the number of character in password
						wrong_pattern	: 	"|<title>.*?(Log In)\s*<\/title>|",  // need to set pattern for wrong password
					}
    in above config user need to ser the wrong pattern so that script come to know where it has to be stop.
    
2.  Other change you need to do in attack function 
     var data = {   
                    username: 'abc_def', // changes password every time
                    password: pwd 
                }; 

    In above configuration you need to change to key as well as the value of usernane. Key name can be found 
    from FORM of login page by VIEW SOURCE or INSPECTING ELEMENT.

3.  3rd and final change is at success of attck function . you need to set the condition to check response 
    is valid or invalid. you can use wrong pattern regex.

NOTE: 
1. This script won't work in secured system like Minimum attemp of password.
2. System in which this script is going to run that system should have high configuration. 
